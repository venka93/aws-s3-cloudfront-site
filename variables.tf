###
# Required
###
variable "certificate_arn" {
  description = "Amazon Resource Name (arn) for the site's certificate"
}

variable "FQDN" {
  description = "Fully qualified domain name for site being hosted"
}

variable "ci_username" {
  description = "IAM user for CI/CD pipeline to access S3 bucket and Cloudfront cache invalidation"
}

variable "responsible_party" {
  description = "Person (pid) who is primarily responsible for the configuration and maintenance of this resource"
}

variable "vcs" {
  description = "A link to the repo in a version control system (usually Git) that manages this resource."
}

###
# Optional
###
variable "compliance_risk" {
  description = "Should be `none`, `ferpa` or `pii`"
  default     = "none"
}

variable "data_risk" {
  description = "Should be `low`, `medium` or `high` based on data-risk classifications defined in VT IT Policies"
  default     = "low"
}

variable "documentation" {
  description = "Link to documentation and/or history file"
  default     = "none"
}

variable "environment" {
  description = "e.g. `development`, `test`, or `production`"
  default     = "production"
}

variable "error_document" {
  description = "Error page being served from S3 bucket"
  default     = "error.html"
}

variable "index_document" {
  description = "Home page being served from S3 bucket"
  default     = "index.html"
}

variable "minimum_protocol_version" {
  description = "Security policy for cloudfront to use for HTTPS connections"
  default     = "TLSv1"
}

variable "responsible_party2" {
  description = "Backup for responsible_party"
  default     = "none"
}

variable "service_name" {
  description = "The high level service this resource is primarily supporting"
  default     = "static-site-hosting"
}

variable "skip_www_alias" {
  description = "Skip the inclusion of the www.FQDN alias on the distribution"
  default     = false
}

variable "versioning_enabled" {
  description = "Versioning for the objects in the S3 bucket"
  default     = false
}

locals {
  domain_aliases = var.skip_www_alias ? [var.FQDN] : ["www.${var.FQDN}", var.FQDN]

  service_tags = {
    Name              = var.FQDN
    Service           = var.service_name
    Environment       = var.environment
    ResponsibleParty  = var.responsible_party
    ResponsibleParty2 = var.responsible_party2
    DataRisk          = var.data_risk
    ComplianceRisk    = var.compliance_risk
    Documentation     = var.documentation
    Comments          = "static site hosting for ${var.FQDN}"
    VCS               = var.vcs
  }
}
