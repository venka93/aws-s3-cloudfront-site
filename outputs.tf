output "s3_bucket" {
  value = {
    name = aws_s3_bucket.S3.id
    arn  = aws_s3_bucket.S3.arn
  }
  description = "Details of the S3 bucket created to host the site content"
}

output "ci_user" {
  value = {
    name = aws_iam_user.iam.name
    arn  = aws_iam_user.iam.arn
  }
  description = "Details about the IAM user created to allow bucket syncing/distribution invalidation in CI builds"
}

output "cloudfront_distribution" {
  value = {
    id             = aws_cloudfront_distribution.distribution.id
    arn            = aws_cloudfront_distribution.distribution.arn
    domain_name    = aws_cloudfront_distribution.distribution.domain_name
    hosted_zone_id = aws_cloudfront_distribution.distribution.hosted_zone_id
    aliases        = local.domain_aliases
  }
  description = "Details about the CloudFront distribution created to serve the site content"
}
